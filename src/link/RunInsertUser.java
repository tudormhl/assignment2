package link;

import controller.InsertUserController;
import view.InsertUserFrame;

public class RunInsertUser {
	
	private InsertUserController myController;
	private InsertUserFrame myView;

	public RunInsertUser(){

			myController = new InsertUserController();
			myView = myController.getView();
			//tell View about Controller 
			myView.addController(myController);	
		}

}
