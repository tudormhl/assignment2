package link;

import controller.DelUpdBookController;
import view.DelUpdBook;

public class RunDelUpdBook {

	
	private DelUpdBookController myController;
	private DelUpdBook myView;

	public RunDelUpdBook() {

			myController = new DelUpdBookController();
			myView = myController.getView();
			//tell View about Controller 
			myView.addController(myController);	
		}
	
}
