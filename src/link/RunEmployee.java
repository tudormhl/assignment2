package link;

import controller.EmployeeController;
import view.EmployeeFrame;

public class RunEmployee {

	private EmployeeController myController;
	private EmployeeFrame myView;

	public RunEmployee(){

			myController = new EmployeeController();
			myView = myController.getView();
			//tell View about Controller 
			myView.addController(myController);	
		}
	
}
