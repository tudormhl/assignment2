package link;

import controller.LoginController;
import view.Login;

public class RunLogin {

	
	private LoginController myController;
	private Login myView;

	public RunLogin() {

			myController = new LoginController();
			myView = myController.getView();
			//tell View about Controller 
			myView.addController(myController);	
		}
	
	
}
