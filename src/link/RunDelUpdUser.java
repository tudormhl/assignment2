package link;


import controller.DelUpdUserController;

import view.DelUpdUser;

public class RunDelUpdUser {

	private DelUpdUserController myController;
	private DelUpdUser myView;

	public RunDelUpdUser() {

			myController = new DelUpdUserController();
			myView = myController.getView();
			//tell View about Controller 
			myView.addController(myController);	
		}
}
