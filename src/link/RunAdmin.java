package link;

import controller.AdminController;
import view.AdminFrame;


public class RunAdmin {

	private AdminController myController;
	private AdminFrame myView;

	public RunAdmin(){

			myController = new AdminController();
			myView = myController.getView();
			//tell View about Controller 
			myView.addController(myController);	
		}
	
}
