package link;

import controller.InsertBookController;
import view.InsertBookFrame;

public class RunInsertBook {

	private InsertBookController myController;
	private InsertBookFrame myView;

	public RunInsertBook(){

			myController = new InsertBookController();
			myView = myController.getView();
			//tell View about Controller 
			myView.addController(myController);	
		}
	
}
