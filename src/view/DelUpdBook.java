package view;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class DelUpdBook extends JFrame {

	private JFrame frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	JLabel lblId;
	JLabel lblTitle;
	JLabel lblAuthor;
	JLabel lblQuantity;
	JLabel lblYear;
	JButton btnDelete;
	JButton btnUpdatet;
	JButton btnUpdatea;
	JButton btnUpdatey;
	JButton btnUpdateq;
	private JTextField textField_5;


	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public DelUpdBook() {
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.frame.getContentPane().setLayout(new BorderLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(100, 26, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(100, 57, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(100, 127, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(100, 158, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(100, 203, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		lblId = new JLabel("ID");
		lblId.setBounds(10, 29, 46, 14);
		contentPane.add(lblId);
		
		lblTitle = new JLabel("Title");
		lblTitle.setBounds(10, 60, 46, 14);
		contentPane.add(lblTitle);
		
		lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(10, 130, 46, 14);
		contentPane.add(lblAuthor);
		
		lblYear = new JLabel("Year");
		lblYear.setBounds(10, 161, 46, 14);
		contentPane.add(lblYear);
		
		lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(10, 206, 46, 14);
		contentPane.add(lblQuantity);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(242, 25, 89, 23);
		contentPane.add(btnDelete);
		
		btnUpdatet = new JButton("UpdateT");
		btnUpdatet.setBounds(242, 73, 89, 23);
		contentPane.add(btnUpdatet);
		
		btnUpdatea = new JButton("UpdateA");
		btnUpdatea.setBounds(242, 126, 89, 23);
		contentPane.add(btnUpdatea);
		
		btnUpdatey = new JButton("UpdateY");
		btnUpdatey.setBounds(242, 157, 89, 23);
		contentPane.add(btnUpdatey);
		
		btnUpdateq = new JButton("UpdateQ");
		btnUpdateq.setBounds(242, 202, 89, 23);
		contentPane.add(btnUpdateq);
		
		this.frame.getContentPane().add(this.contentPane,BorderLayout.CENTER);
		
		textField_5 = new JTextField();
		textField_5.setBounds(100, 88, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNewTitle = new JLabel("New Title");
		lblNewTitle.setBounds(10, 91, 46, 14);
		contentPane.add(lblNewTitle);
		
	
		this.frame.validate();
	}
	
	public void addController(ActionListener controller){
		this.btnDelete.addActionListener(controller);
		this.btnUpdatet.addActionListener(controller);
		this.btnUpdatea.addActionListener(controller);
		this.btnUpdatey.addActionListener(controller);
		this.btnUpdateq.addActionListener(controller);
	}
	
	public JButton getBtnDelete() {
		return btnDelete;
	}
	
	public JButton getBtnUpdateT() {
		return btnUpdatet;
	}
	
	public JButton getBtnUpdateA() {
		return btnUpdatea;
	}
	
	public JButton getBtnUpdateY() {
		return btnUpdatey;
	}
	
	public JButton getBtnUpdateQ() {
		return btnUpdateq;
	}

	public String getTextField_ID() {
		return textField.getText();
	}
	
	public String getTextField_Title() {
		return textField_1.getText();
	}
	
	public String getTextField_Author() {
		return textField_2.getText();
	}
	
	public String getTextField_Year() {
		return textField_3.getText();
	}
	
	public String getTextField_Quantity() {
		return textField_4.getText();
	}
	
	public String getTextField_NewTitle() {
		return textField_5.getText();
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public void setVisiblity(Boolean visibil) {
		this.frame.setVisible(visibil);		
	}	
}
