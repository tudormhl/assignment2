package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class DelUpdUser extends JFrame {

	private JFrame frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton btnUpdateu;
	private JButton btnDelete;
	private JButton btnUpdatep;
	private JLabel lblId;
	private JLabel lblUsername;
	private JLabel lblPassword;
	private JTextField textField_3;
	private JLabel lblNewUser;


	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public DelUpdUser() {
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.frame.getContentPane().setLayout(new BorderLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(100, 37, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(100, 119, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(100, 163, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		lblId = new JLabel("ID");
		lblId.setBounds(20, 40, 46, 14);
		contentPane.add(lblId);
		
		lblUsername = new JLabel("Username");
		lblUsername.setBounds(20, 122, 59, 14);
		contentPane.add(lblUsername);
		
		lblPassword = new JLabel("Password");
		lblPassword.setBounds(20, 166, 46, 14);
		contentPane.add(lblPassword);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(244, 36, 89, 23);
		contentPane.add(btnDelete);
		
		btnUpdateu = new JButton("UpdateU");
		btnUpdateu.setBounds(244, 102, 89, 23);
		contentPane.add(btnUpdateu);
		
		btnUpdatep = new JButton("UpdateP");
		btnUpdatep.setBounds(244, 162, 89, 23);
		contentPane.add(btnUpdatep);
		
		this.frame.getContentPane().add(this.contentPane,BorderLayout.CENTER);
		
		textField_3 = new JTextField();
		textField_3.setBounds(100, 88, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		lblNewUser = new JLabel("New User");
		lblNewUser.setBounds(20, 91, 59, 14);
		contentPane.add(lblNewUser);
		
		
		this.frame.validate();
	}
	
	public void addController(ActionListener controller){
		this.btnDelete.addActionListener(controller);
		this.btnUpdateu.addActionListener(controller);
		this.btnUpdatep.addActionListener(controller);
	
	}
	
	public JButton getBtnDelete(){
		return btnDelete;
	}
	
	public JButton getBtnUpdateu() {
		return btnUpdateu;
	}
	
	public JButton getBtnUpdatep() {
		return btnUpdatep;
	}
	
	public String getTextField_ID() {
		return textField.getText();
	}
	
	public String getTextField_Username() {
		return textField_1.getText();
	}
	
	public String getTextField_Password() {
		return textField_2.getText();
	}
	
	public String getTextField_NewUser() {
		return textField_3.getText();
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}

	public void setVisiblity(Boolean visibil) {
		this.frame.setVisible(visibil);		
	}	
}
