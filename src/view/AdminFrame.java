package view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

public class AdminFrame extends JFrame {

	/**
	 * 
	 */
	private JFrame frame;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnInsertBook;
	private JButton btnDeleteupdateBook;
	private JButton btnInsertUser;
	private JButton btnDeleteupdateUser;
	private JButton btnOutOfStock;
	
	

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public AdminFrame() {
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.frame.setLayout(new BorderLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		btnInsertBook = new JButton("Insert Book");
		btnInsertBook.setBounds(42, 48, 89, 23);
		contentPane.add(btnInsertBook);
		
		btnDeleteupdateBook = new JButton("Delete/Update Book");
		btnDeleteupdateBook.setBounds(183, 48, 142, 23);
		contentPane.add(btnDeleteupdateBook);
		
		btnInsertUser = new JButton("Insert User");
		btnInsertUser.setBounds(42, 110, 89, 23);
		contentPane.add(btnInsertUser);
		
		btnDeleteupdateUser = new JButton("Delete/Update User");
		btnDeleteupdateUser.setBounds(183, 110, 142, 23);
		contentPane.add(btnDeleteupdateUser);
		
		btnOutOfStock = new JButton("Out of stock Report");
		btnOutOfStock.setBounds(105, 164, 142, 23);
		contentPane.add(btnOutOfStock);
		
		this.frame.add(this.contentPane,BorderLayout.CENTER);
		this.frame.validate();
	}
	
	public void addController(ActionListener controller){
		this.btnInsertBook.addActionListener(controller);
		this.btnInsertUser.addActionListener(controller);
		this.btnDeleteupdateBook.addActionListener(controller);
		this.btnDeleteupdateUser.addActionListener(controller);
		this.btnOutOfStock.addActionListener(controller);
	}
	
	public JButton getBtnInsertBook() {
		return btnInsertBook;
	}
	
	public JButton getBtnInsertUser() {
		return btnInsertUser;
	}
	
	public JButton getBtnDelUpdBook() {
		return btnDeleteupdateBook;
	}
	
	public JButton getBtnDelUpdUser() {
		return btnDeleteupdateUser;
	}
	
	public JButton getBtnOutStock() {
		return btnOutOfStock;
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public void setVisiblity(Boolean visibil) {
		this.frame.setVisible(visibil);		
	}	

}
