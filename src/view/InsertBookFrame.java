package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

public class InsertBookFrame extends JFrame {

	
	private JFrame frame ;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JButton btnInsert;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public InsertBookFrame() {
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.frame.setLayout(new BorderLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		/////////////this.frame.setContentPane(contentPane); NU face asta ptr ca merge ciudat. la final de contructor
		// faci this.frame.add(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(103, 26, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(103, 57, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(103, 88, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(103, 119, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(103, 150, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(103, 181, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(103, 212, 86, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(24, 91, 46, 14);
		contentPane.add(lblAuthor);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(24, 60, 46, 14);
		contentPane.add(lblTitle);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(24, 122, 46, 14);
		contentPane.add(lblYear);
		
		JLabel lblGenre = new JLabel("Genre");
		lblGenre.setBounds(24, 153, 46, 14);
		contentPane.add(lblGenre);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(24, 184, 46, 14);
		contentPane.add(lblQuantity);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(24, 215, 46, 14);
		contentPane.add(lblPrice);
		
		btnInsert = new JButton("Insert");
		btnInsert.setBounds(262, 118, 89, 23);
		contentPane.add(btnInsert);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(24, 29, 46, 14);
		contentPane.add(lblId);
		
		this.frame.add(this.contentPane,BorderLayout.CENTER);
		this.frame.validate();
	}
	
	public void addController(ActionListener controller){
		this.btnInsert.addActionListener(controller);	
	}
	
	public JButton getInsertBtn() {
		return btnInsert;
	}
	
	public String getTextField_ID() {
		return textField.getText();
	}
	
	public String getTextField_Title() {
		return textField_1.getText();
	}
	
	public String getTextField_Author() {
		return textField_2.getText();
	}
	
	public String getTextField_Year() {
		return textField_3.getText();
	}
	
	public String getTextField_Genre() {
		return textField_4.getText();
	}
	
	public String getTextField_Quantity() {
		return textField_5.getText();
	}
	
	public String getTextField_Price() {
		return textField_6.getText();
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public void setVisiblity(Boolean visibil) {
		this.frame.setVisible(visibil);		
	}	
	
	

}
