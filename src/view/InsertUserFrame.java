package view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class InsertUserFrame extends JFrame {

	private JFrame frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JButton btnInsert;
	
	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public InsertUserFrame() {
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.frame.getContentPane().setLayout(new BorderLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(113, 93, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(113, 138, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(113, 185, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblUsername = new JLabel("username");
		lblUsername.setBounds(22, 96, 65, 14);
		contentPane.add(lblUsername);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(22, 48, 46, 14);
		contentPane.add(lblId);
		
		JLabel lblPassword = new JLabel("password");
		lblPassword.setBounds(22, 141, 46, 14);
		contentPane.add(lblPassword);
		
		JLabel lblRole = new JLabel("Role");
		lblRole.setBounds(22, 188, 46, 14);
		contentPane.add(lblRole);
		
		btnInsert = new JButton("Insert");
		btnInsert.setBounds(273, 113, 89, 23);
		contentPane.add(btnInsert);
		
		this.frame.getContentPane().add(this.contentPane,BorderLayout.CENTER);
		
		textField = new JTextField();
		textField.setBounds(113, 45, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		this.frame.validate();
	}
	
	public void addController(ActionListener controller){
		this.btnInsert.addActionListener(controller);
		
	}
	
	public JButton getBtnInsert() {
		return btnInsert;
	}
	
	public String getTextField_ID() {
		return textField.getText();
	}
	
	public String getTextField_Username() {
		return textField_1.getText();
	}
	
	public String getTextField_Password() {
		return textField_2.getText();
	}
	
	public String getTextField_Role() {
		return textField_3.getText();
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public void setVisiblity(Boolean visibil) {
		this.frame.setVisible(visibil);		
	}	
}
