package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import model.Book;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class EmployeeFrame extends JFrame {
	
	public void fillView(List<Book> books){
		//=============== setez tabelul
		 String[] headColoane = { "Clientii :"};
		 DefaultTableModel dtm = new DefaultTableModel(0, headColoane.length);
		 this.table.setVisible(true);
		 this.table.setModel(dtm);
		 this.table.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 250), 1, true));
		 this.table.setFont(new java.awt.Font("Arial", 0, 12));
		 dtm.addRow(new Object[] { "Books :" });
		 dtm.addRow(new Object[] {""});
		 for(Book  carte : books){
			dtm.addRow(new Object[] {carte.toString()});
		}
    }

	private JTable table;
	private JFrame frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JButton btnSearchtitle;
	private JButton btnSearchauthor;
	private JButton btnSearchyear;
	private JButton btnSell;

	
	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public EmployeeFrame() {
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.frame.getContentPane().setLayout(new BorderLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(108, 59, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		btnSearchtitle = new JButton("SearchTitle");
		btnSearchtitle.setBounds(235, 27, 99, 23);
		contentPane.add(btnSearchtitle);
		
		btnSearchauthor = new JButton("SearchAuthor");
		btnSearchauthor.setBounds(235, 58, 99, 23);
		contentPane.add(btnSearchauthor);
		
		textField_2 = new JTextField();
		textField_2.setBounds(108, 90, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		btnSearchyear = new JButton("SearchYear");
		btnSearchyear.setBounds(235, 92, 99, 23);
		contentPane.add(btnSearchyear);
		
		textField_3 = new JTextField();
		textField_3.setBounds(108, 121, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(108, 152, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(22, 31, 46, 14);
		contentPane.add(lblTitle);
		
		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(22, 62, 46, 14);
		contentPane.add(lblAuthor);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(22, 93, 46, 14);
		contentPane.add(lblYear);
		
		JLabel lblTitlesell = new JLabel("Title-Sell");
		lblTitlesell.setBounds(22, 124, 46, 14);
		contentPane.add(lblTitlesell);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(22, 155, 46, 14);
		contentPane.add(lblQuantity);
		
		btnSell = new JButton("Sell");
		btnSell.setBounds(235, 135, 99, 23);
		contentPane.add(btnSell);
		
		this.frame.getContentPane().add(this.contentPane,BorderLayout.CENTER);
		
		textField = new JTextField();
		textField.setBounds(108, 28, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		table = new JTable();
		table.setBounds(22, 181, 402, 69);
		contentPane.add(table);
		this.frame.validate();
	}
	
	public void addController(ActionListener controller){
		this.btnSearchtitle.addActionListener(controller);
		this.btnSearchauthor.addActionListener(controller);
		this.btnSearchyear.addActionListener(controller);
		this.btnSell.addActionListener(controller);
	}
	
	public JButton getBtnSearchT() {
		return btnSearchtitle;
	}
	
	public JButton getBtnSearchA() {
		return btnSearchauthor;
	}
	
	public JButton getBtnSearchY() {
		return btnSearchyear;
	}
	
	public JButton getBtnSell() {
		return btnSell;
	}
	
	public String getTextField_Title() {
		return textField.getText();
	}
	
	public String getTextField_Author() {
		return textField_1.getText();
	}
	
	public String getTextField_Year() {
		return textField_2.getText();
	}
	
	public String getTextField_TitleSell() {
		return textField_3.getText();
	}
	
	public String getTextField_Quantity() {
		return textField_4.getText();
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public void setVisiblity(Boolean visibil) {
		this.frame.setVisible(visibil);		
	}	
}
