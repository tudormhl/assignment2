package view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class Login extends JFrame {

	/**
	 * 
	 */
	private JFrame frame;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblUsername;
	private JLabel lblPassword;
	private JButton btnLogin;
	

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public Login() {
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.frame.getContentPane().setLayout(new BorderLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		
		textField = new JTextField();
		textField.setBounds(133, 55, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(133, 98, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		lblUsername = new JLabel("username");
		lblUsername.setBounds(30, 58, 72, 14);
		contentPane.add(lblUsername);
		
		lblPassword = new JLabel("password");
		lblPassword.setBounds(30, 101, 58, 14);
		contentPane.add(lblPassword);
		
		btnLogin = new JButton("Login");
		btnLogin.setBounds(201, 164, 89, 23);
		contentPane.add(btnLogin);
		
		this.frame.getContentPane().add(this.contentPane,BorderLayout.CENTER);

		this.frame.validate();
	}
	
	public void addController(ActionListener controller){
		this.btnLogin.addActionListener(controller);
		
	}
	
	public JButton getBtnLogin(){
		return btnLogin;
	}
	
	public String getTextField_Username() {
		return textField.getText();
	}
	
	public String getTextField_Pass() {
		return textField_1.getText();
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public void setVisiblity(Boolean visibil) {
		this.frame.setVisible(visibil);		
	}	
}
