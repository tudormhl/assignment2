package controller;

import java.awt.event.ActionEvent;

import org.xml.sax.SAXException;

import link.RunAdmin;
import model.User;
import model.UserOperation;
import view.InsertUserFrame;

public class InsertUserController implements java.awt.event.ActionListener{

	private InsertUserFrame insertUser;
	private UserOperation uo;
	
	public InsertUserController(){
		this.insertUser = new InsertUserFrame();
		this.uo = new UserOperation();
	}
	
	public InsertUserFrame getView(){
		return this.insertUser;
	}
	
	public void actionPerformed(ActionEvent e){
		if(insertUser.getBtnInsert() == e.getSource())
		{
			
			int id;
			String username, password,type;
			
			try{
			id = Integer.parseInt(insertUser.getTextField_ID());
			username = insertUser.getTextField_Username();
			password = insertUser.getTextField_Password();
			type = insertUser.getTextField_Role();
			
			User user = new User(id,username,password,type);
			uo.insertEntry(user);
			}
			catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				insertUser.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} catch (SAXException e1) {
				System.out.println(e1);
				insertUser.displayErrorMessage("Inregistrarea NU s-a realizat cu succes."); 
		} 
			insertUser.setVisiblity(false);
			new RunAdmin();
			
	}	
	}
}
