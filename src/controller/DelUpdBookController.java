package controller;

import java.awt.event.ActionEvent;

import link.RunAdmin;
import model.BookOperation;
import view.DelUpdBook;

public class DelUpdBookController implements java.awt.event.ActionListener{
	
	private DelUpdBook delUpdBook;
	private BookOperation bo;
	
	public DelUpdBookController()
	{
		this.delUpdBook = new DelUpdBook();
		this.bo = new BookOperation();
	}
	
	public DelUpdBook getView(){
		return this.delUpdBook;
	}
	
	public void actionPerformed(ActionEvent e){
		if(delUpdBook.getBtnDelete() == e.getSource())
		{
			int id;
			try{
				id = Integer.parseInt(delUpdBook.getTextField_ID());
				
				bo.deleteEntry(id);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdBook.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} 
			delUpdBook.setVisiblity(false);
			new RunAdmin();
		}
		
		if(delUpdBook.getBtnUpdateA() == e.getSource())
		{
			String author,title;
			try{
				author = delUpdBook.getTextField_Author();
				title = delUpdBook.getTextField_Title();
				bo.updateAuthor(title, author);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdBook.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} 
			delUpdBook.setVisiblity(false);
			new RunAdmin();
		}
		
		if(delUpdBook.getBtnUpdateT() == e.getSource())
		{
			String title1,title;
			try{
				title1 = delUpdBook.getTextField_NewTitle();
				title = delUpdBook.getTextField_Title();
				bo.updateAuthor(title, title1);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdBook.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			}
			delUpdBook.setVisiblity(false);
			new RunAdmin();
		}
		
		if(delUpdBook.getBtnUpdateY() == e.getSource())
		{
			String year,title;
			try{
				year = delUpdBook.getTextField_Year();
				title = delUpdBook.getTextField_Title();
				bo.updateAuthor(title, year);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdBook.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} 
			delUpdBook.setVisiblity(false);
			new RunAdmin();
		}
		
		if(delUpdBook.getBtnUpdateQ() == e.getSource())
		{
			String quantity,title;
			try{
				quantity = delUpdBook.getTextField_Quantity();
				title = delUpdBook.getTextField_Title();
				bo.updateAuthor(title, quantity);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdBook.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
		}
			delUpdBook.setVisiblity(false);
			new RunAdmin();
	}

	}
}
