package controller;

import java.awt.event.ActionEvent;

import link.RunAdmin;
import model.UserOperation;
import view.DelUpdUser;

public class DelUpdUserController implements java.awt.event.ActionListener{

	private DelUpdUser delUpdUser;
	private UserOperation uo;
	
	public DelUpdUserController()
	{
		this.delUpdUser = new DelUpdUser();
		this.uo = new UserOperation();
	}
	
	public DelUpdUser getView(){
		return this.delUpdUser;
	}
	
	public void actionPerformed(ActionEvent e){
		if(delUpdUser.getBtnDelete() == e.getSource())
		{
			int id;
			try{
				id = Integer.parseInt(delUpdUser.getTextField_ID());
				
				uo.deleteEntry(id);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdUser.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} 
			delUpdUser.setVisiblity(false);
			new RunAdmin();
		}
		
		if(delUpdUser.getBtnUpdateu() == e.getSource())
		{
			String user,user1;
			try{
				user = delUpdUser.getTextField_Username();
				user1 = delUpdUser.getTextField_NewUser();
				uo.updateUsername(user, user1);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdUser.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} 
			delUpdUser.setVisiblity(false);
			new RunAdmin();
		}
		
		if(delUpdUser.getBtnUpdatep() == e.getSource())
		{
			String title1,title;
			try{
				title1 = delUpdUser.getTextField_Username();
				title = delUpdUser.getTextField_Password();
				uo.updatePassword(title1, title);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				delUpdUser.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} 
			delUpdUser.setVisiblity(false);
			new RunAdmin();
		}
		
	}
	
}
