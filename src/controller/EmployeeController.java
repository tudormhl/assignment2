package controller;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import model.Book;
import model.BookOperation;
import view.EmployeeFrame;



public class EmployeeController implements java.awt.event.ActionListener{

private EmployeeFrame admin;
private BookOperation bo;
	
	public EmployeeController(){
		this.admin = new EmployeeFrame();	
		this.bo = new BookOperation();
	
	}
	
	public EmployeeFrame getView(){
		return this.admin;
	}
	
	public void actionPerformed(ActionEvent e){
		if(this.admin.getBtnSearchA() == e.getSource()){
			System.out.println("rahat cu sana");
			String author;
			try{
				author = admin.getTextField_Author();
				admin.fillView(bo.findByAuthor(author));
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				admin.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
		}
		}
		
		if(this.admin.getBtnSearchT() == e.getSource()){
			String author;
			List<Book> lista = new ArrayList<Book>();
			try{
				author = admin.getTextField_Title();
				lista.add(bo.findByTitle(author));
				admin.fillView(lista);
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				admin.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
		}
		}
		
		if(this.admin.getBtnSearchY() == e.getSource()){
			String author;
			try{
				author = admin.getTextField_Year();
				admin.fillView(bo.findByYear(author));
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				admin.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
		}
		}
		
		if(this.admin.getBtnSell() == e.getSource()){
			String title;
			int quantity;
			try{
				title = admin.getTextField_TitleSell();
				quantity = Integer.parseInt(admin.getTextField_Quantity());
				Book book = bo.findByTitle(title);
				int dif = book.getQuantity()-quantity;
				bo.updateQuantity(title, dif+"");
				
			}catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				admin.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
		}
		}
		
	}
	
}
