package controller;

import java.awt.event.ActionEvent;

import link.RunDelUpdBook;
import link.RunDelUpdUser;
import link.RunInsertBook;
import link.RunInsertUser;
import model.Book;
import model.BookOperation;
import model.Report;
import model.ReportOperation;
import view.AdminFrame;

public class AdminController implements java.awt.event.ActionListener{
	
	private AdminFrame admin;
	private BookOperation bo;
	private ReportOperation ro;
	
	public AdminController(){
		this.admin = new AdminFrame();	
		this.bo = new BookOperation();
		this.ro = new ReportOperation();
	
	}
	
	public AdminFrame getView(){
		return this.admin;
	}
	
	public void actionPerformed(ActionEvent e){
		if(this.admin.getBtnInsertBook() == e.getSource()){
			admin.setVisiblity(false);
			new RunInsertBook();
		}
		
		if(this.admin.getBtnInsertUser() == e.getSource()){
			admin.setVisiblity(false);
			new RunInsertUser();
		}
		
		if(this.admin.getBtnDelUpdBook() == e.getSource()){
			admin.setVisiblity(false);
			new RunDelUpdBook();
		}
		
		if(this.admin.getBtnDelUpdUser() == e.getSource()){
			admin.setVisiblity(false);
			new RunDelUpdUser();
		}
		
		if(this.admin.getBtnOutStock() == e.getSource()){

			
			
		}
	}

}
