package controller;

import java.awt.event.ActionEvent;

import org.xml.sax.SAXException;

import link.RunAdmin;
import model.Book;
import model.BookOperation;
import view.InsertBookFrame;

public class InsertBookController implements java.awt.event.ActionListener{
	
	private BookOperation bo;
	private InsertBookFrame insertBook;
	
	public InsertBookController(){
		this.insertBook = new InsertBookFrame();
	    this.bo = new BookOperation();
	}
	
	public InsertBookFrame getView(){
		return this.insertBook;
	}
	
	public void actionPerformed(ActionEvent e){
		if(insertBook.getInsertBtn() == e.getSource())
		{
			System.out.print("rahat cu sana");
			int id,quantity, price;
			String title, author,year, genre;
			
			try{
			id = Integer.parseInt(insertBook.getTextField_ID());
			title = insertBook.getTextField_Title();
			author = insertBook.getTextField_Author();
			year = insertBook.getTextField_Year();
			genre = insertBook.getTextField_Genre();
			quantity = Integer.parseInt(insertBook.getTextField_Quantity());
			price = Integer.parseInt(insertBook.getTextField_Price());
			
			Book book = new Book();
			book.setId(id);
			book.setTitle(title);
			book.setAuthor(author);
			book.setYear(year);
			book.setGenre(genre);
			book.setQuantity(quantity);
			book.setPrice(price);
			bo.insertEntry(book);
			}
			catch(NullPointerException | NumberFormatException ex){ 
				System.out.println(ex);
				insertBook.displayErrorMessage("Datele intorduse nu sunt corecte!Verificati din nou!"); 
			} catch (SAXException e1) {
				System.out.println(e1);
				insertBook.displayErrorMessage("Inregistrarea NU s-a realizat cu succes."); 
		} 
			insertBook.setVisiblity(false);
			new RunAdmin();
			
	}	
	}
	
	
}
