package controller;

import java.awt.event.ActionEvent;

import link.RunAdmin;
import link.RunEmployee;
import model.User;
import model.UserCatalog;
import view.Login;

public class LoginController implements java.awt.event.ActionListener{

	private Login login;

	private UserCatalog uc;

	
	public LoginController(){
		this.login = new Login();	
	}
	
	public Login getView(){
		return this.login;
	}
	
	public void actionPerformed(ActionEvent e){
		if(this.login.getBtnLogin() == e.getSource()){
			String username;
			String password;
					
			uc = UserCatalog.getInstance();
			try{
				username = login.getTextField_Username();
				password = login.getTextField_Pass();
				User u = uc.Login(username, password);
				if (u.getType().equals("employee")){
					new RunEmployee();
				}
				else {

					new RunAdmin();
				}
			}catch(NullPointerException ex){ 
				System.out.println(ex);
				login.displayErrorMessage("Userul si/sau parola nu sunt corecte"); 
			}   
		}
	}
}
