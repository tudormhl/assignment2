package model;

public class Report {

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	private int id;
	private String report;
	private String data;
	
	public Report(int id, String report,  String data){
		this.id = id;
		this.report = report;
		this.data = data;
	}
	
}
