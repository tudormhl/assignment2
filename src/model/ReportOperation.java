package model;

import java.io.*;
import java.util.*;

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;


public class ReportOperation {

	public void insertEntryOutOfStock(Report report) throws SAXException {
		try {

			String filepath = "D:/work-eclipse/Assignment2/xmlFiles/xml_files/OutOfStock.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element company = doc.getRootElement();
			Element reportSelling = new Element("report");
			reportSelling.setAttribute("id", report.getId() + "");

			Element mesaj = new Element("message");
			mesaj.addContent(new Text(report.getReport()));

			reportSelling.addContent(mesaj);

			company.addContent(reportSelling);

			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
public List<String> readReportXML(String reportType) {
		
		List<String> listReports = new ArrayList<>();
		
		try {
			String filepath = "D:/work-eclipse/Assignment2/xmlFiles/xml_files/OutOfStock.xml";
			
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element reportsstore = doc.getRootElement();
			List<Element> reports = reportsstore.getChildren();

			for (int i = 0; i < reports.size(); i++) {
				Element staff = reports.get(i);
				// obtinere elemente
				Integer id = staff.getAttribute("id").getIntValue();
				List<Element> noduri = staff.getChildren();
				String mesaj = noduri.get(0).getValue();
				String data = noduri.get(1).getValue();
				
				// creare obiect carte
				Report report = new Report(id,mesaj,data);
				
				listReports.add(report.toString());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return listReports;
	}

}
