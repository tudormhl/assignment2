package model;

public class Book {

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setId(int price) {
		this.price = price;
	}
	
	public int getId() {
		return id;
	}

	public void setPrice(int id) {
		this.id = id;
	}

	private int id;
	private String title;
	private String author;
	private String year;
	private String genre;
	private int quantity;
	private int price;

	public Book(){
	
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	public String toString(){
		return "Title: "+title+"|Author: "+author+"|Year: "+year+"|Genre: "+genre+"|Quantity: "+quantity+"|Price: "+price;
	}
	
	
}
