package model;

import java.io.*;
import java.util.*;


import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

public class BookOperation {
	
	public List<Book> readBookXML() {
		List<Book> listBooks = new ArrayList<>();
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element bookstore = doc.getRootElement();
			List<Element> books = bookstore.getChildren();

			for (int i = 0; i < books.size(); i++) {
				Element staff = books.get(i);
				// obtinere elemente
				Integer id = staff.getAttribute("id").getIntValue();
				List<Element> noduri = staff.getChildren();
				String title = noduri.get(0).getValue();
				String author = noduri.get(1).getValue();
				String year = noduri.get(2).getValue();
				String genre = noduri.get(3).getValue();
				int quantity = Integer.parseInt(noduri.get(4).getValue());
				int price = Integer.parseInt(noduri.get(5).getValue());
				// creare obiect carte
				Book book = new Book();
				book.setId(id);
				book.setTitle(title);
				book.setAuthor(author);
				book.setYear(year);
				book.setGenre(genre);
				book.setQuantity(quantity);
				book.setPrice(price);

				listBooks.add(book);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return listBooks;
	}
	
	public void insertEntry(Book book) throws SAXException {
		try {

			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element company = doc.getRootElement();
			Element carte = new Element("book");
			carte.setAttribute("id", book.getId() + "");

			Element title = new Element("title");
			title.addContent(new Text(book.getTitle()));
			Element author = new Element("author");
			author.addContent(new Text(book.getAuthor()));
			Element year = new Element("year");
			year.addContent(new Text(book.getYear() + ""));
			Element genre = new Element("genre");
			genre.addContent(new Text(book.getGenre()));
			Element quantity = new Element("quantity");
			quantity.addContent(new Text(book.getQuantity() + ""));
			Element price = new Element("price");
			price.addContent(new Text(book.getPrice() + ""));

			carte.addContent(title);
			carte.addContent(author);
			carte.addContent(year);
			carte.addContent(genre);
			carte.addContent(quantity);
			carte.addContent(price);
			company.addContent(carte);

			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteALL() {
		
		try{
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);
			
			Element bookstore = doc.getRootElement();
			bookstore.removeContent();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteEntry(int id) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element bookstore = doc.getRootElement();
			List<Element> staffs = bookstore.getChildren();
			for (int i = 0; i < staffs.size() - 1; i++) {
				Element staff = staffs.get(i);
				Integer idd = staff.getAttribute("id").getIntValue();
				if (idd.equals(id)) {
					bookstore.removeContent(staff);
					//staff.detach();
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public void updateTitle(String titluCurent, String titluNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> staffs = bookstore.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(titluCurent)) {
						noduri.get(j).removeContent();
						noduri.get(j).addContent(titluNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	public void updateAuthor(String titluCarte, String autorNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> staffs = bookstore.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size() - 1; j++) {
					if ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(titluCarte)) {
						noduri.get(j + 1).removeContent();
						noduri.get(j + 1).addContent(autorNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	public void updateYear(String titluCarte, String anNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> staffs = bookstore.getChildren();

			for (int i = 0; i < staffs.size() - 1; i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size() - 2; j++) {
					if ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(titluCarte)) {
						noduri.get(j + 2).removeContent();
						noduri.get(j + 2).addContent(anNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	public void updateGenre(String titluCarte, String genreNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> staffs = bookstore.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size() - 3; j++) {
					if ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(titluCarte)) {
						noduri.get(j + 3).removeContent();
						noduri.get(j + 3).addContent(genreNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	public void updateQuantity(String titluCarte, String quantityNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> staffs = bookstore.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size() - 4; j++) {
					if ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(titluCarte)) {
						noduri.get(j + 4).removeContent();
						noduri.get(j + 4).addContent(quantityNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	public void updatePrice(String titluCarte, String priceNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> staffs = bookstore.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size() - 5; j++) {
					if ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(titluCarte)) {
						noduri.get(j + 5).removeContent();
						noduri.get(j + 5).addContent(priceNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public Book findByTitle(String titlu) {
		Book book = new Book();
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			;
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> books = bookstore.getChildren();

			for (int i = 0; i < books.size(); i++) {
				Element staff = books.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ("title".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(titlu)) {
						Element cautat = staff;
						// obtinere elemente
						Integer id = staff.getAttribute("id").getIntValue();
						List<Element> nodurile = cautat.getChildren();
						String title = nodurile.get(0).getValue();
						String author = nodurile.get(1).getValue();
						String year = nodurile.get(2).getValue();
						String genre = nodurile.get(3).getValue();
						int quantity = Integer.parseInt(nodurile.get(4).getValue());
						int price = Integer.parseInt(nodurile.get(5).getValue());
						// creare obiect carte
						book.setId(id);
						book.setTitle(title);
						book.setAuthor(author);
						book.setYear(year);
						book.setGenre(genre);
						book.setQuantity(quantity);
						book.setPrice(price);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return book;
	}

	public List<Book> findByAuthor(String autor) {
		List<Book> carti = new ArrayList<>();
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			;
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> books = bookstore.getChildren();

			for (int i = 0; i < books.size(); i++) {
				Element staff = books.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ("author".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(autor)) {
						Element cautat = staff;
						// obtinere elemente
						Integer id = staff.getAttribute("id").getIntValue();
						List<Element> nodurile = cautat.getChildren();
						String title = nodurile.get(0).getValue();
						String author = nodurile.get(1).getValue();
						String year = nodurile.get(2).getValue();
						String genre = nodurile.get(3).getValue();
						int quantity = Integer.parseInt(nodurile.get(4).getValue());
						int price = Integer.parseInt(nodurile.get(5).getValue());
						// creare obiect carte
						Book book = new Book();
						book.setId(id);
						book.setTitle(title);
						book.setAuthor(author);
						book.setYear(year);
						book.setGenre(genre);
						book.setQuantity(quantity);
						book.setPrice(price);
						carti.add(book);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return carti;
	}

	public List<Book> findByYear(String an) {
		List<Book> carti = new ArrayList<>();
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			;
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> books = bookstore.getChildren();

			for (int i = 0; i < books.size(); i++) {
				Element staff = books.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ("year".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(an)) {
						Element cautat = staff;
						// obtinere elemente
						Integer id = staff.getAttribute("id").getIntValue();
						List<Element> nodurile = cautat.getChildren();
						String title = nodurile.get(0).getValue();
						String author = nodurile.get(1).getValue();
						String year = nodurile.get(2).getValue();
						String genre = nodurile.get(3).getValue();
						int quantity = Integer.parseInt(nodurile.get(4).getValue());
						int price = Integer.parseInt(nodurile.get(5).getValue());
						// creare obiect carte
						Book book = new Book();
						book.setId(id);
						book.setTitle(title);
						book.setAuthor(author);
						book.setYear(year);
						book.setGenre(genre);
						book.setQuantity(quantity);
						book.setPrice(price);
						carti.add(book);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return carti;
	}
	
	public List<Book> findOutOfStock() {
		List<Book> carti = new ArrayList<>();
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/Books.xml";
			;
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element bookstore = doc.getRootElement();
			List<Element> books = bookstore.getChildren();

			for (int i = 0; i < books.size(); i++) {
				Element staff = books.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ("quantity".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals("0")) {
						Element cautat = staff;
						// obtinere elemente
						Integer id = staff.getAttribute("id").getIntValue();
						List<Element> nodurile = cautat.getChildren();
						String title = nodurile.get(0).getValue();
						String author = nodurile.get(1).getValue();
						String year = nodurile.get(2).getValue();
						String genre = nodurile.get(3).getValue();
						int quantity = Integer.parseInt(nodurile.get(4).getValue());
						int price = Integer.parseInt(nodurile.get(5).getValue());
						// creare obiect carte
						Book book = new Book();
						book.setId(id);
						book.setTitle(title);
						book.setAuthor(author);
						book.setYear(year);
						book.setGenre(genre);
						book.setQuantity(quantity);
						book.setPrice(price);
						carti.add(book);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return carti;
	}
}
