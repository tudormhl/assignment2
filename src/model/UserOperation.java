package model;

import java.io.*;
import java.util.*;


import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

public class UserOperation {

	public List<User> readBookXML() {
		List<User> listUsers = new ArrayList<>();
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/User.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element users = doc.getRootElement();
			List<Element> staffs = users.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ("type".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals("employee") || 
							noduri.get(j).getValue().equals("administrator")){
						// obtinere elemente
						Integer id = staff.getAttribute("id").getIntValue();
						List<Element> nodurile = staff.getChildren();
						String username = nodurile.get(0).getValue();
						String password = nodurile.get(1).getValue();
						String type = nodurile.get(2).getValue();
						// creare obiect user
						User user = new User(id,username,password,type);
					
						listUsers.add(user);
					}
			}
		 }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return listUsers;
	}
	
	public void deleteEntry(int id) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/User.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element users = doc.getRootElement();
			List<Element> staffs = users.getChildren();
			for (int i = 0; i < staffs.size() - 1; i++) {
				Element staff = staffs.get(i);
				Integer idd = staff.getAttribute("id").getIntValue();
				if (idd.equals(id)) {
					users.removeContent(staff);
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	// =============================================== INSERT DATA
	public void insertEntry(User user) throws SAXException {
		try {

			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/User.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fis);

			Element company = doc.getRootElement();
			Element staff = new Element("staff");
			staff.setAttribute("id", user.getId() + "");

			Element username = new Element("username");
			username.addContent(new Text(user.getUsername()));
			Element password = new Element("password");
			password.addContent(new Text(user.getPassword()));
			Element type = new Element("type");
			type.addContent(new Text(user.getType()));

			staff.addContent(username);
			staff.addContent(password);
			staff.addContent(type);
			company.addContent(staff);

			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
	
	public void updateUsername(String usernameCurent, String usernameNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/User.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element users = doc.getRootElement();
			List<Element> staffs = users.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size(); j++) {
					if ("username".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(usernameCurent)) {
						noduri.get(j).removeContent();
						noduri.get(j).addContent(usernameNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	public void updatePassword(String usernameCurent, String passwordNou) {
		try {
			String filepath = "E:/faculta/AN II/workspace/Assignment2PS/User.xml";
			File xmlFile = new File(filepath);
			FileInputStream fis = new FileInputStream(xmlFile);
			SAXBuilder sb = new SAXBuilder();

			Document doc = sb.build(fis);
			Element users = doc.getRootElement();
			List<Element> staffs = users.getChildren();

			for (int i = 0; i < staffs.size(); i++) {
				Element staff = staffs.get(i);
				List<Element> noduri = staff.getChildren();
				for (int j = 0; j < noduri.size() - 1; j++) {
					if ("username".equals(noduri.get(j).getName()) && noduri.get(j).getValue().equals(usernameCurent)) {
						noduri.get(j + 1).removeContent();
						noduri.get(j + 1).addContent(passwordNou);
					}
				}
			}
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileOutputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}
}
