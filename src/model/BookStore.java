package model;

import java.util.*;

public class BookStore {
	
   public List<Book> getStore() {
		return store;
	}

	public void setStore(List<Book> store) {
		this.store = store;
	}

private List<Book> store;	
   private BookOperation bo;
	
	private  BookStore(){
		this.store = bo.readBookXML();
	}
	
	private static BookStore instance = new BookStore();

	public static BookStore getInstance() {
		return instance;
	}
	
	public void addBook(Book b){
		store.add(b);
	}
	
	public void removeBook(Book b){
		store.remove(b);
	}
	
	public int getIndex(String name){

		for (Book b: store){
			if (b.getTitle() == name)
				return store.indexOf(b);
		}
		return 0;
	}
	
	public void updateName(String name, String name1){
		store.get(getIndex(name)).setTitle(name1);
	}
	
	public void updateYear(String name, String year){
		store.get(getIndex(name)).setYear(year);
	}
	
	public void updateAuthor(String name, String name1){
		store.get(getIndex(name)).setAuthor(name1);
	}
	
	public Book searchByTitle(String title)
	{
		for(Book b: store){
			if (b.getTitle() == title)
				return b;
		}
		
		return null;
	}
	
	public List<Book> searchByYear(String year)
	{
		List<Book> store1 = new ArrayList<Book>();
		for(Book b: store){
			if (b.getYear() == year)
				store1.add(b);
		}
		
		return store1;
	}
	
	public List<Book> searchByAuthor(String author){
		List<Book> store1 = new ArrayList<Book>();
		for(Book b: store){
			if (b.getAuthor() == author)
				store1.add(b);
		}
		return store1;
	}
	
	public List<Book> OutofStock(){
		List<Book> store1 = new ArrayList<Book>();
		for(Book b: store){
			if (b.getQuantity() == 0)
				store1.add(b);
		}
		return store1;
	}
}
