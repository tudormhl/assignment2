package model;


import java.util.List;

public class UserCatalog {
	
	   private List<User> users;	
	   private UserOperation uo;
		
		private  UserCatalog(){
			this.uo = new UserOperation();
			this.users = uo.readBookXML();
		}
		
		private static UserCatalog instance = new UserCatalog();

		public static UserCatalog getInstance() {
			return instance;
		}
	
	public void addUser(User user)
	{
		users.add(user);
	}
	
	public List<User> getList(){
		return users;		
	}
	
	public void deleteUser(User user)
	{
		users.remove(user);
	}
	
	public int getIndex(int id)
	{
		for(User u: users)
			if(u.getId() == id)
				return users.indexOf(u);
		return 0;
	}
	
	public void updateUser(int id, String username)
	{
		users.get(getIndex(id)).setUsername(username);
	}
	
	public void updatePass(int id, String password)
	{
		users.get(getIndex(id)).setPassword(password);
	}
	
	public User Login(String username,String password)
	{
		for(User u1: users){
			if((u1.getUsername().equals(username)) && (u1.getPassword().equals(password)))
				return u1;
			
		}
		return null;
	}

}
